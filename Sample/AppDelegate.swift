//
//  AppDelegate.swift
//  Sample
//
//  Created by Rafay Misfit on 19/5/20.
//  Copyright © 2020 Misfit. All rights reserved.
//

import UIKit
import PushKit
import CallKit
import JitsiMeet
import CallerSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {


    var window: UIWindow?
    var callerUuid = UUID()
    var callerIdForBGMode = String()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let register = JitsiCaller.ctreaeJitsiCaller()
        window?.rootViewController = register
        window?.makeKeyAndVisible()
        
        guard let launchOptions = launchOptions else { return false }
        return JitsiMeet.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//        return true
    }

    func voipRegistration() {
        let mainQuew = DispatchQueue.main
        let voipRegistory: PKPushRegistry = PKPushRegistry(queue: mainQuew)
        voipRegistory.delegate = self
        voipRegistory.desiredPushTypes = [.voIP]
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //print("APNs token retrieved: \(deviceToken)")
        //Messaging.messaging().apnsToken = deviceToken
        self.voipRegistration()
        
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return JitsiMeet.sharedInstance().application(application, continue: userActivity, restorationHandler: restorationHandler)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return JitsiMeet.sharedInstance().application(app, open: url, options: options)
    }
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate: PKPushRegistryDelegate {
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        let tokenParts = pushCredentials.token.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        
        if type == .voIP {
            let dataDict:[String: String] = ["token": token]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            PreferencesUtils.saveStringData(data: token, dataName: Constants.FCM_TOKEN)
        }
    }

    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {

        //let payloadDict = payload.dictionaryPayload as? Dictionary<String, String>

        if let data = payload.dictionaryPayload as? [String: AnyObject], let callerId = data["call"] as? String {
            self.callerIdForBGMode = callerId
            switch UIApplication.shared.applicationState {
            case .active:
                let request: CallAcknowledgementRequestModel = CallAcknowledgementRequestModel(method: Constants.CALL_ACKNOWLEDGEMENT, params: CallAckParams(uid: PreferencesUtils.getSavedStringData(dataName: Constants.UID), cid: callerId))
                
                SocketUtils.shared.requestForAcknowledgement(requestModel: request, onSuccess: { (response) in
                    let data = ["id": response?.result?.data?.id, "from": response?.result?.data?.from, "name": response?.result?.data?.name, "type": response?.result?.data?.type]
                    //callerName = response?.result?.data?.from
                    
                    if(response?.method == Constants.CALL_ACTION) {
                        
                        NotificationCenter.default.post(name: Notification.Name("pushinactivemode"), object: self, userInfo: data as [AnyHashable : Any])
                    }
                    
                }) { (error) in
                }
            default:
                let config = CXProviderConfiguration(localizedName: "My App")
                //config.iconTemplateImageData = UIImagePNGRepresentation(UIImage(named: "pizza")!)
                config.ringtoneSound = "ringtone.caf"
                config.includesCallsInRecents = false;
                config.supportsVideo = true;
                let provider = CXProvider(configuration: config)
                provider.setDelegate(self, queue: nil)
                let update = CXCallUpdate()
                update.remoteHandle = CXHandle(type: .generic, value: "Caller SDK")
                update.hasVideo = true
                self.callerUuid = UUID()
                SocketUtils.shared.callerID = self.callerUuid
                provider.reportNewIncomingCall(with: self.callerUuid, update: update, completion: { error in
                    SocketUtils.shared.connectionClose()
                    SocketUtils.shared.connectToSocket()
                })
            }
        }
    }

    func pushRegistry(registry: PKPushRegistry!, didInvalidatePushTokenForType type: String!) {
        NSLog("token invalidated")
    }
}

extension AppDelegate: CXProviderDelegate {
    
    func providerDidReset(_ provider: CXProvider) {
        
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        action.fulfill()
        
        let request: CallAcknowledgementRequestModel = CallAcknowledgementRequestModel(method: Constants.CALL_ACKNOWLEDGEMENT, params: CallAckParams(uid: PreferencesUtils.getSavedStringData(dataName: Constants.UID), cid: self.callerIdForBGMode))
        
        SocketUtils.shared.requestForAcknowledgement(requestModel: request, onSuccess: { (response) in
            let data = ["id": response?.result?.data?.id, "from": response?.result?.data?.from, "name": response?.result?.data?.name, "type": response?.result?.data?.type, "forBackgroundMode": "yes", "acceptCallFromBG": "yes"]
            //callerName = response?.result?.data?.from
            
            if(response?.method == Constants.CALL_ACTION) {
                NotificationCenter.default.post(name: Notification.Name("pushinactivemode"), object: self, userInfo: data as [AnyHashable : Any])
            }
            
        }) { (error) in
        }
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        action.fulfill()
        
        let request: CallAcknowledgementRequestModel = CallAcknowledgementRequestModel(method: Constants.CALL_ACKNOWLEDGEMENT, params: CallAckParams(uid: PreferencesUtils.getSavedStringData(dataName: Constants.UID), cid: self.callerIdForBGMode))
        
        SocketUtils.shared.requestForAcknowledgement(requestModel: request, onSuccess: { (response) in
            let data = ["id": response?.result?.data?.id, "from": response?.result?.data?.from, "name": response?.result?.data?.name, "type": response?.result?.data?.type, "forBackgroundMode": "yes"]
            //callerName = response?.result?.data?.from
            
            if(response?.method == Constants.CALL_ACTION) {
                
                NotificationCenter.default.post(name: Notification.Name("pushinactivemode"), object: self, userInfo: data as [AnyHashable : Any])
            }
            
        }) { (error) in
        }
    }
}


